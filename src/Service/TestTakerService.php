<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Service;

use App\Service\Parser\ParserFactory;
use App\VO\TestTaker;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class TestTakerService
 *
 * @package App\Service
 */
class TestTakerService
{
    /** @var Serializer */
    private $serializer;

    /** @var string */
    private $fixturesDir;

    public function __construct()
    {
        $this->serializer = new Serializer(
            [new ObjectNormalizer()],
            []
        );
    }

    /**
     * @param string $publicDir
     * @return TestTakerService
     */
    public function withPublicDir(string $publicDir): self
    {
        $this->fixturesDir = $publicDir . 'fixtures/';
        return $this;
    }

    /**
     * @return array
     */
    public function fetchDataSources(): array
    {
        if (!$this->fixturesDir) {
            throw new \LogicException("no data sources provided.");
        }

        try {
            /** @var Finder $finder */
            $finder = (new Finder())->files()->in($this->fixturesDir);
        } catch (\Exception $e) {
            throw new \LogicException("unable to fetch data from data source.");
        }

        $takers = [];
        foreach ($finder as $file) {
            $path = $file->getPathname();

            $parser = ParserFactory::getParser($path);
            $takers += array_merge($takers, $parser->parseFromPath($path));
        }

        return array_map(function(array $taker) {
            return $this->serializer->denormalize($taker, TestTaker::class);
        }, $takers);
    }
}
