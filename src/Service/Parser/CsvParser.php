<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Service\Parser;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class CsvParser
 *
 * @package App\Service\Parser
 */
class CsvParser implements ParserInterface
{
    /** @var Serializer */
    private $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer(
            [],
            [new CsvEncoder()]
        );
    }

    /**
     * @inheritdoc
     */
    public function parseFromPath(string $path): array
    {
        if (!file_exists($path)) {
            throw new \LogicException("file {$path} does not exists. please check the path is correct.");
        }

        return $this->serializer->decode(file_get_contents($path), 'csv');
    }
}
