<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Service\Parser;

/**
 * Class ParserFactory
 *
 * @package App\Service\Parser
 */
class ParserFactory
{
    /**
     * @param string $path
     * @return ParserInterface
     */
    public static function getParser(string $path): ParserInterface
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        switch (strtolower($ext)) {
            case 'csv':
                return new CsvParser();
            case 'json':
                return new JsonParser();
        }

        throw new \LogicException("no parser for file extension `{$ext}`.");
    }
}
