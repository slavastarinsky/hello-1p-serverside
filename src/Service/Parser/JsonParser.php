<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Service\Parser;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class JsonParser
 *
 * @package App\Service\Parser
 */
class JsonParser implements ParserInterface
{
    /** @var Serializer */
    private $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer(
            [],
            [new JsonEncoder()]
        );
    }

    /**
     * @inheritdoc
     */
    public function parseFromPath(string $path): array
    {
        if (!file_exists($path)) {
            throw new \LogicException("file {$path} does not exists. please check the path is correct.");
        }

        return $this->serializer->decode(file_get_contents($path), 'json');
    }
}
