<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Service\Parser;

/**
 * Interface ParserInterface
 *
 * @package App\Service\Parser
 */
interface ParserInterface
{
    /**
     * @param string $path
     * @return array
     */
    public function parseFromPath(string $path): array;
}
