<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ApiController
 *
 * @package App\Controller\Api
 */
class ApiController extends AbstractController
{
    const DEFAULT_RESPONSE_HEADERS = [
        'Content-type'                 => 'application/json',
        'Access-Control-Allow-Origin'  => '*',
        'Access-Control-Allow-Headers' => '*'
    ];

    /**
     * @param $data
     * @return Response
     */
    protected function response($data): Response
    {
        $serializer = new Serializer(
            [new ObjectNormalizer()],
            [new JsonEncoder()]
        );

        return new Response(
            $serializer->serialize(['data' => $data], 'json'),
            200,
            self::DEFAULT_RESPONSE_HEADERS
        );
    }

    /**
     * @param \Throwable $e
     * @return Response
     */
    protected function error(\Throwable $e): Response
    {
        return new JsonResponse([
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
//            'trace' => $e->getTrace()
        ], 500, self::DEFAULT_RESPONSE_HEADERS);
    }
}
