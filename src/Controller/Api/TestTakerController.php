<?php
/**
 * @author S <slava.starinsky@spiralscout.com>
 */

namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Service\TestTakerService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TestTakerController
 *
 * @package App\Controller\Api
 */
class TestTakerController extends ApiController
{
    /**
     * @Route("/api/takers/list.json", methods={"GET"}, format="json", name="list")
     *
     * @param TestTakerService $service
     * @return Response
     */
    public function getListAction(TestTakerService $service): Response
    {
        try {
            $result = $service
                ->withPublicDir($this->getParameter('kernel.project_dir') . '/public/')
                ->fetchDataSources();
        } catch (\Exception $e) {
            return $this->error($e);
        }

        return $this->response($result);
    }
}
